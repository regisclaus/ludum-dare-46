﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

enum Turn
{
    PLAYER,
    ENEMY
}

[Serializable]
public class ItemsPerLevel
{
    public Item[] itens;
    public int minLevel;
}

[Serializable]
public class EnemyPerLevel
{
    public Enemy[] enemies;
    public int minLevel;
}


class Character
{
    public string nameCharacter;
    public int maxHealth;
    public int health;
    public int attack;
    public int defense;
    public int hitChance;

    public void SetLevelAndStats(int level)
    {

        this.attack = Random.Range(level, 2 * level + 1);
        this.defense = Random.Range(level, 2 * level + 1);
        this.hitChance = Random.Range(level+1, 2 * level + 1);
    }

    public void SetHealth(int minHealth, int maxHealth)
    {
        this.maxHealth = Random.Range(minHealth, maxHealth+1);
        this.health = this.maxHealth;
    }

    public void EquipWeapon(Item item)
    {
        attack += item.attack;
        defense += item.defense;
        hitChance += item.hitChance;
    }

    public void DesequipItem(Item item)
    {
        attack -= item.attack;
        defense -= item.defense;
        hitChance -= item.hitChance;
    }
}

public class GameManager : MonoBehaviour
{
    private int level;
    private int turnCount;
    private bool batttleFinished;
    private Character player;
    private Character enemy;
    private Turn turn;
    private string feedbackText;
    private int minHealth = 100;

    private Item[] weaponsSorted = new Item[3];
    private int weaponIndexUsed = -1;

    private Item[] potionsSorted = new Item[3];
    private int potionIndexUsed = -1;

    private int playerStatsPoints = 1;
    private int enemyStatsPoints = 1;

    private string playerColor = "04c524";
    private string enemyColor = "e30808";

    [Header("General Properties")]
    [SerializeField] private float turnTime = 3f;
    [SerializeField] private int startLevel = 1;
    [SerializeField] private int startStatsPoints = 1;
    [SerializeField] private int startMinHealth = 100;
    [SerializeField] private int deltaMaxHealth = 50;

    [Header("Battle Properties")]
    [SerializeField] private int damageMinMult = 10;
    [SerializeField] private int damageRangeMult = 10;
    [SerializeField] private int defenseMinMult = 5;
    [SerializeField] private int defenseRangeMult = 2;
    [SerializeField] private int hitChanceMult = 20;

    [Header("General Components")]
    [SerializeField] private TMP_Text turnFeedback;
    [SerializeField] private Button restartButton;
    [SerializeField] private TMP_Text levelText;
    [SerializeField] private Button startBattleButton;

    [Header("Player Components")]
    [SerializeField] private Image playerHealthBar;
    [SerializeField] private TMP_Text playerHealthText;
    [SerializeField] private TMP_Text playerAttackText;
    [SerializeField] private TMP_Text playerDefenseText;
    [SerializeField] private TMP_Text playerHitChanceText;
    [SerializeField] private Button playerAttackButton;
    [SerializeField] private Button playerDefenseButton;
    [SerializeField] private Button playerHitChanceButton;
    [SerializeField] private TMP_Text playerStatsPointsText;
    [SerializeField] private Image playerImage;

    [Header("Enemy Components")]
    [SerializeField] private Image enemyHealthBar;
    [SerializeField] private TMP_Text enemyHealthText;
    [SerializeField] private TMP_Text enemyAttackText;
    [SerializeField] private TMP_Text enemyDefenseText;
    [SerializeField] private TMP_Text enemyHitChanceText;
    [SerializeField] private Button enemyAttackButton;
    [SerializeField] private Button enemyDefenseButton;
    [SerializeField] private Button enemyHitChanceButton;
    [SerializeField] private TMP_Text enemyStatsPointsText;
    [SerializeField] private Image enemyImage;
    [SerializeField] private EnemyPerLevel[] enemisPerLevel;

    [Header("Player Itens Components")]
    [SerializeField] private ItemsPerLevel[] weaponPerLevel;
    [SerializeField] private Button weapon1Button;
    [SerializeField] private Button weapon2Button;
    [SerializeField] private Button weapon3Button;

    [Header("Enemy Itens Components")]
    [SerializeField] private ItemsPerLevel[] potionsPerLevel;
    [SerializeField] private Button potion1Button;
    [SerializeField] private Button potion2Button;
    [SerializeField] private Button potion3Button;

    [Header("Sounds Effects")]
    [SerializeField] private AudioSource se;
    [SerializeField] private AudioClip startBattleSound;
    [SerializeField] private AudioClip playerAttackSound;
    [SerializeField] private AudioClip enemyAttackSound;
    [SerializeField] private AudioClip equipWeaponSound;
    [SerializeField] private AudioClip equipPotionSound;
    [SerializeField] private AudioClip winSound;
    [SerializeField] private AudioClip loseSound;
    [SerializeField] private AudioClip prepareSound;
    [SerializeField] private AudioClip upSound;
    [SerializeField] private AudioClip downSound;

    private void Feedback(string text)
    {
        turnFeedback.text = text;
    }

    private void Update()
    {
        UpdatePlayerStats();
        UpdateEnemyStats();

       // startBattleButton.interactable = playerStatsPoints <= 0 && enemyStatsPoints <= 0;
    }

    private void UpdatePlayerStats()
    {
        levelText.text = $"{level}";
        playerAttackText.text = $"{player.attack}";
        playerDefenseText.text = $"{player.defense}";

        playerHitChanceText.text = player.hitChance < level ? $"<color=#d20a0a>" : $"<color=#00ABFF>";
        playerHitChanceText.text += $"{player.hitChance}</color>";

        playerHealthText.text = $"{player.health}/{player.maxHealth}";
        playerHealthBar.fillAmount = (float)player.health/ (float)player.maxHealth;

        playerStatsPointsText.text = playerStatsPoints > 0 ? $"<color=#ffdb00>" : $"<color=#a5a5a5>";
        playerStatsPointsText.text += $"{playerStatsPoints}</color>";

        if (playerStatsPoints < 1)
        {
            DisablePlayerButtons();
        }

    }

    private void DisablePlayerButtons()
    {
        playerAttackButton.interactable = false;
        playerDefenseButton.interactable = false;
        playerHitChanceButton.interactable = false;
    }

    private void EnablePlayerButtons()
    {
        playerAttackButton.interactable = true;
        playerDefenseButton.interactable = true;
        playerHitChanceButton.interactable = true;
    }

    public void UpdateEnemyStats()
    {
        enemyAttackText.text = $"{enemy.attack}";
        enemyDefenseText.text = $"{enemy.defense}";
        enemyHitChanceText.text = enemy.hitChance < level ? $"<color=#d20a0a>" : $"<color=#00ABFF>";
        enemyHitChanceText.text += $"{enemy.hitChance}</color>";

        enemyHealthText.text = $"{enemy.health}/{enemy.maxHealth}";
        enemyHealthBar.fillAmount = (float)enemy.health / (float)enemy.maxHealth;

        enemyStatsPointsText.text = enemyStatsPoints > 0 ? $"<color=#ffdb00>" : $"<color=#a5a5a5>";
        enemyStatsPointsText.text += $"{enemyStatsPoints}</color>";

        if (enemyStatsPoints < 1)
        {
            DisableEnemyButtons();
        }
}

    private void DisableEnemyButtons()
    {
        enemyAttackButton.interactable = false;
        enemyDefenseButton.interactable = false;
        enemyHitChanceButton.interactable = false;
    }
    private void EnableEnemyButtons()
    {
        enemyAttackButton.interactable = enemy.attack > 1;
        enemyDefenseButton.interactable = enemy.defense > 1;
        enemyHitChanceButton.interactable = enemy.hitChance > 1;
    }


    private void Start()
    {
        level = startLevel;
        playerStatsPoints = startStatsPoints;
        enemyStatsPoints = startStatsPoints;
        minHealth = startMinHealth;

        DesequipWeapon();
        DesequipPotion();
        weaponIndexUsed = -1;
        potionIndexUsed = -1;
        DefineWeaponsAndPotions();

        CreateCharacters();

        Feedback($"<color=#ffdb00>1)</color> Choose your [weapon] and stats to [update]. \n" +
            $"<color=#ffdb00>2)</color> Choose a [potion] and stats to [downgrade] your enemy. \n" +
            $"<color=#ffdb00>3)</color> Click in [Battle] button and keep it alive.");

        EnablePlayerButtons();
        EnableEnemyButtons();
        startBattleButton.interactable = true;

        se.PlayOneShot(prepareSound);
    }



    private void DefineWeaponsAndPotions()
    {
        // Weapons
        int index = 0;
        for(index = weaponPerLevel.Length - 1; index >= 0; index--)
        {
            if(weaponPerLevel[index].minLevel <= level)
            {
                break;
            } 
        }

        weaponsSorted[0] = weaponPerLevel[index].itens[0];
        weaponsSorted[1] = weaponPerLevel[index].itens[1];
        weaponsSorted[2] = weaponPerLevel[index].itens[2];

        weapon1Button.image.sprite = weaponsSorted[0].icon;
        weapon2Button.image.sprite = weaponsSorted[1].icon;
        weapon3Button.image.sprite = weaponsSorted[2].icon;

        // Potions
        index = 0;
        for (index = potionsPerLevel.Length - 1; index >= 0; index--)
        {
            if (potionsPerLevel[index].minLevel <= level)
            {
                break;
            }
        }

        potionsSorted[0] = potionsPerLevel[index].itens[0];
        potionsSorted[1] = potionsPerLevel[index].itens[1];
        potionsSorted[2] = potionsPerLevel[index].itens[2];

        potion1Button.image.sprite = potionsSorted[0].icon;
        potion2Button.image.sprite = potionsSorted[1].icon;
        potion3Button.image.sprite = potionsSorted[2].icon;

    }

    private void CreateCharacters()
    {
        CreatePlayer();
        CreateEnemy(1);
    }

    private void CreatePlayer()
    {
        player = new Character();
        player.nameCharacter = "You";
        player.SetHealth(minHealth, minHealth);
        player.SetLevelAndStats(1);
    }

    private void CreateEnemy(int level)
    {

        int minLevelIndex = 0;
        for (minLevelIndex = enemisPerLevel.Length - 1; minLevelIndex >= 0; minLevelIndex--)
        {
            if (enemisPerLevel[minLevelIndex].minLevel <= level)
            {
                break;
            }
        }



        int randomIndex = Random.Range(0, 3);


        enemy = new Character();
        enemy.nameCharacter = enemisPerLevel[minLevelIndex].enemies[randomIndex].nameEnemy;
        enemyImage.sprite = enemisPerLevel[minLevelIndex].enemies[randomIndex].icon;
        enemy.SetHealth(minHealth, minHealth + deltaMaxHealth);
        enemy.SetLevelAndStats(level);
    }

    public void StartBattle()
    {
        startBattleButton.interactable = false;

        batttleFinished = false;
        turnCount = 1;

        DisablePlayerButtons();
        DisableEnemyButtons();

        Feedback($"Battle Started!");

        StartCoroutine(RunTurn());

        se.PlayOneShot(startBattleSound);
    }

    IEnumerator RunTurn()
    {
        while(!batttleFinished)
        {
            yield return new WaitForSeconds(turnTime);

            if (VerifyEndBattle())
                FinishBattle();
            else
            {
                
                feedbackText = $"Turn: ";
                feedbackText += turn == Turn.PLAYER
                    ? $"<color=#{playerColor}>{player.nameCharacter}</color>\n"
                    : $"<color=#{enemyColor}>{enemy.nameCharacter}</color>\n";

                if (turn == Turn.PLAYER)
                    TryAttack(player, enemy);
                else
                    TryAttack(enemy, player);

                Feedback(feedbackText);
                PassTurn();
            }


        }
    }


    private void TryAttack(Character attacker, Character opponent)
    {
        float chanceTried = Random.Range(0, 101);
        int chance = attacker.hitChance >= level ? 100 : 100 - (level - attacker.hitChance) * hitChanceMult;
        if (chanceTried < chance)
            Attack(attacker, opponent);
        else
            feedbackText += $"\n{attacker.nameCharacter} miss.";
    }

    private void Attack(Character attacker, Character opponent)
    {
        int damage = Random.Range(attacker.attack * damageMinMult, attacker.attack * damageMinMult + attacker.attack * damageRangeMult);
        int damageDefended = Mathf.Min(Random.Range(opponent.defense * defenseMinMult, opponent.defense * defenseMinMult + opponent.defense * defenseRangeMult), damage);
        int realDamage = damage - damageDefended;
        opponent.health = Mathf.Max(0, opponent.health - realDamage);

        feedbackText += $"\n{attacker.nameCharacter} hits {damage} points.";
        feedbackText += $"\n{opponent.nameCharacter} defend {damageDefended} points.";
        feedbackText += $"\n{opponent.nameCharacter} loss <color=#";
        feedbackText += turn == Turn.PLAYER ? enemyColor : playerColor;
        feedbackText += $">{realDamage}</color></b> of health points";

        if (turn == Turn.PLAYER)
        {
            playerImage.GetComponent<Animator>().SetTrigger("Attack");
            se.PlayOneShot(playerAttackSound);
        }
        else
        {
            enemyImage.GetComponent<Animator>().SetTrigger("Attack");
            se.PlayOneShot(enemyAttackSound);
        }
    }

    private void FinishBattle()
    {
        DefineVictory();
        batttleFinished = true;
    }

    private void DefineVictory()
    {
        if (player.health <= 0)
        {
            CanRestartGame();
        }
        else if (enemy.health <= 0)
        {
            WinBattle();
        }
    }

    private void WinBattle()
    {
        Feedback("Congratulations! The enemy died! Go to next battle.");

        level++;
        playerStatsPoints += level;
        enemyStatsPoints += level;
        minHealth = minHealth + level * deltaMaxHealth;
        
        player.SetHealth(minHealth, minHealth);
        CreateEnemy(level);

        DesequipWeapon();
        DesequipPotion();
        weaponIndexUsed = -1;
        potionIndexUsed = -1;
        DefineWeaponsAndPotions();

        // UI
        EnablePlayerButtons();
        EnableEnemyButtons();
        startBattleButton.interactable = true;

        se.PlayOneShot(winSound);

    }

    private void CanRestartGame()
    {
        Feedback("You died!");
        restartButton.gameObject.SetActive(true);
        startBattleButton.gameObject.SetActive(false);

        se.PlayOneShot(loseSound);
    }

    private bool VerifyEndBattle()
    {
        return player.health <= 0 || enemy.health <= 0;
    }

    private void PassTurn()
    {
        turn = turn == Turn.PLAYER ? Turn.ENEMY : Turn.PLAYER;
        turnCount++;
    }

    public void RestartGame()
    {
        Start();

        startBattleButton.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(false);
    }


    // Itens

    public void EquipWeapon(int index)
    {
        DesequipWeapon();

        weaponIndexUsed = index;
        player.EquipWeapon(weaponsSorted[weaponIndexUsed]);

        switch(index)
        {
            case 0:
                weapon1Button.image.color = new Color(1, 1, 1, 1f) ;
                break;
            case 1:
                weapon2Button.image.color = new Color(1, 1, 1, 1f);
                break;
            case 2:
                weapon3Button.image.color = new Color(1, 1, 1, 1f);
                break;
        }

        se.PlayOneShot(equipWeaponSound);
    }

    public void EquipPotion(int index)
    {
        DesequipPotion();

        potionIndexUsed = index;
        enemy.EquipWeapon(potionsSorted[potionIndexUsed]);

        switch (index)
        {
            case 0:
                potion1Button.image.color = new Color(1, 1, 1, 1f);
                break;
            case 1:
                potion2Button.image.color = new Color(1, 1, 1, 1f);
                break;
            case 2:
                potion3Button.image.color = new Color(1, 1, 1, 1f);
                break;
        }

        se.PlayOneShot(equipPotionSound);
    }

    public void DesequipWeapon()
    {
        if (weaponIndexUsed != -1)
            player.DesequipItem(weaponsSorted[weaponIndexUsed]);

        weapon1Button.image.color = new Color(1, 1, 1, 0.5f);
        weapon2Button.image.color = new Color(1, 1, 1, 0.5f);
        weapon3Button.image.color = new Color(1, 1, 1, 0.5f);
    }

    public void DesequipPotion()
    {
        if (potionIndexUsed != -1)
            enemy.DesequipItem(potionsSorted[potionIndexUsed]);

        potion1Button.image.color = new Color(1, 1, 1, 0.5f);
        potion2Button.image.color = new Color(1, 1, 1, 0.5f);
        potion3Button.image.color = new Color(1, 1, 1, 0.5f);
    }


    // Ups Buttons

    public void UpAttackPlayer()
    {
        player.attack++;
        playerStatsPoints--;
        se.PlayOneShot(upSound);
    }

    public void UpDefensePlayer()
    {
        player.defense++;
        playerStatsPoints--;
        se.PlayOneShot(upSound);
    }

    public void UpHitChancePlayer()
    {
        player.hitChance++;
        playerStatsPoints--;
        se.PlayOneShot(upSound);
    }

    // Down Buttons

    public void DownAttackEnemy()
    {
        enemy.attack--;
        enemyAttackButton.interactable = enemy.attack > 1;
        enemyStatsPoints--;
        se.PlayOneShot(downSound);
    }

    public void DownDefenseEnemy()
    {
        enemy.defense--;
        enemyDefenseButton.interactable = enemy.defense > 1;
        enemyStatsPoints--;
        se.PlayOneShot(downSound);
    }

    public void DownHitChanceEnemy()
    {
        enemy.hitChance--;
        enemyHitChanceButton.interactable = enemy.hitChance > 1;
        enemyStatsPoints--;
        se.PlayOneShot(downSound);
    }
}
