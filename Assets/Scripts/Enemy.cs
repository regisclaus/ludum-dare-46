﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{
    public string nameEnemy;
    public Sprite icon;
    public int minLevel;
}
