﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject
{
    public int attack;
    public int defense;
    public int hitChance;
    public Sprite icon;
}
